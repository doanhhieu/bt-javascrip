function checkName() {
  filter = /^[a-zA-Z]/;
  if (filter.test($("#name").val()))
    return true;
}
function validateName() {
  if (checkName()) {
    $('#name').removeClass('error');
    $('#alert-name').text('');
    $('#alert-name').removeClass('error');
    return true;
  } 
  else {
    $('#name').addClass('error');
    $('#alert-name').text('* Tên không hợp lệ');
    $('#alert-name').addClass('error');
    return false;
  }
}

function checkEmail() {
  filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
  if (filter.test($('#email').val()))
    return true;
}

function validateEmail() {
  if (checkEmail()) {
    $('#email').removeClass('error');
    $('#alert-email').text('');
    $('#alert-email').removeClass('error');
    return true;
  } 
  else {
    $('#email').addClass('error');
    $('#alert-email').text('* Email không hợp lệ');
    $('#alert-email').addClass('error');
    return false;
  }
}

function checkNumber() {
  filter = /^[0]+[0-9]/;
  if (filter.test($('#number').val()))
    return true;
}

function validateNumber() {
  if (checkNumber()) {
    $('#number').removeClass('error');
    $('#alert-number').text('');
    $('#alert-number').removeClass('error');
    return true;
  } 
  else {
    $('#number').addClass('error');
    $('#alert-number').text('* Số điện thoại không hợp lệ');
    $('#alert-number').addClass('error');
    return false;
  }
}

function checkCompany() {
  filter = /^[a-zA-Z.]/;
  if (filter.test($('#company').val()))
    return true;
}

function validateCompany() {
  if (checkCompany()) {
    $('#company').removeClass('error');
    $('#alert-company').text('');
    $('#alert-company').removeClass('error');
    return true;
  } 
  else {
    $('#company').addClass('error');
    $('#alert-company').text('* Tên công ty không hợp lệ');
    $('#alert-company').addClass('error');
    return false;
    }
}

function validateForm() {
  if (!checkName() || !checkEmail() || !checkNumber() || !checkCompany())
    return true;
}


$(document).ready(function()
{
  $('#name').blur(validateName);
  $('#email').blur(validateEmail);
  $('#number').blur(validateNumber);
  $('#company').blur(validateCompany);
  $('input').change(function(){
    if(validateForm()) {
      $('#submit').attr('disabled', 'disabled');
    } 
    else {
      $('#submit').removeAttr('disabled');
    }
  });
    
  var submit = $("button[type='submit']");
    submit.click(function(e)
    {
        e.preventDefault();
        var data = $('#form').serialize();
        $.ajax({
        type : 'POST',
        url : 'https://reqres.in/api/users',
        data : data,
        success : function(response){
            alert('Them user thanh cong !');
            console.log(response);
        }
        });
  });
});
